<?php

declare(strict_types=1);

namespace wym\addons;

use DirectoryIterator;
use think\Exception;
use think\Route;
use think\Console;
use think\helper\Str;
use think\facade\Config;
use think\facade\Lang;
use think\facade\Cache;
use think\facade\Event;

/**
 * ============================
 * 插件服务
 *
 * @Author  :   WYM
 * @Version :  1.0
 * @DateTime: 2024-06-18 15:59:39
 * Class Service
 * @package wym\addons
 * ============================
 */
class Service extends \think\Service
{
    // addons 路径
    protected string $addons_path;

    //存放[插件名称]列表数据
    protected array $addons_data = [];

    //存放[插件ini所有信息]列表数据
    protected array $addons_data_list = [];

    //模块所有[config.php]里的信息存放
    protected array $addons_data_list_config = [];

    /**
     * ============================
     * 复制相关目录
     *
     * @param string    $name
     * @param bool|null $delete
     *
     * @Author  :   WYM
     * @Version :  1.0
     * @DateTime: 2024-06-18 16:08:32
     * ============================
     */
    public static function copyFiles(string $name, bool|null $delete = false): void
    {
        $path = self::getDirs();

        foreach ($path as $k => $v) {
            self::recurseCopy(app()->addons->getAddonsPath() . $name . DS . $k . DS . $name, $v . $name, $delete);
        }
    }

    /**
     * ============================
     * 获取需移动的目录
     *
     * @Author  :   WYM
     * @Version :  1.0
     * @DateTime: 2022-11-09 22:43:06
     * ============================
     */
    public static function getDirs(): array
    {
        return [
            'app'     => base_path(),
            'public'  => public_path() . 'static' . DS,
            'uni-app' => public_path() . 'html' . DS,
        ];
    }

    /**
     * ============================
     * 递归复制
     *
     * @param string    $source 源目录
     * @param string    $dest   新目录
     * @param bool|null $delete
     *
     * @Author  :   WYM
     * @Version :  1.0
     * @DateTime: 2024-06-18 16:11:40
     * ============================
     */
    private static function recurseCopy(string $source, string $dest, bool|null $delete = false): void
    {
        if (!is_dir($source)) {
            //如果目标不是一个目录则退出
            return; //退出函数
        }

        if (!is_dir($dest)) {
            mkdir($dest, 0777, true);
        }
        $dir = opendir($source);

        while (false !== ($file = readdir($dir))) {
            if (($file != '.') && ($file != '..')) {
                if (is_dir($source . DS . $file)) {
                    self::recurseCopy($source . DS . $file, $dest . DS . $file, $delete);

                    if ($delete) {
                        rmdir($source . DS . $file);
                    }
                } else {
                    copy($source . DS . $file, $dest . DS . $file);

                    if ($delete) {
                        unlink($source . DS . $file);
                    }
                }
            }
        }
        closedir($dir);
    }

    /**
     * ============================
     * 删除相关目录
     *
     * @param string    $name
     * @param bool|null $delete
     *
     * @Author  :   WYM
     * @Version :  1.0
     * @DateTime: 2024-06-18 16:10:52
     * ============================
     */
    public static function removeFiles(string $name, bool|null $delete = false): void
    {
        $path = self::getDirs();

        foreach ($path as $k => $v) {
            self::recurseCopy($v . $name, app()->addons->getAddonsPath() . $name . DS . $k . DS . $name, $delete);
        }
    }

    /**
     * ============================
     * 更新插件状态
     *
     * @param string $name
     * @param int    $status
     * @param int    $install
     *
     * @Author  :   WYM
     * @Version :  1.0
     * @DateTime: 2024-06-18 16:11:59
     * ============================
     * @throws \think\Exception
     */
    public static function updateAddonsInfo(string $name, int $status = 1, int $install = 1): void
    {
        $addonslist                   = getAddonsList();
        $addonslist[$name]['status']  = $status;
        $addonslist[$name]['install'] = $install;
        Cache::set('addonslist', $addonslist);
        try {
            setAddonsInfo($name, ['status' => $status, 'install' => $install]);
        } catch (Exception) {
            throw new Exception(lang('file does not have write permission'));
        }
    }

    public function register(): void
    {
        $this->app->bind('addons', Service::class);

        // 无则创建addons目录
        $this->addons_path = $this->getAddonsPath();
        // 自动载入插件
        $this->autoload();
        // 加载系统语言包
        $this->loadLang();
        // 2.注册插件事件hook
        $this->loadEvent();
        // 加载插件系统服务
        $this->loadService();
        // 加载插件命令
        $this->loadCommand();
        // 4.自动加载全局的插件内部第三方类库
        $this->vendorAutoload($this->addons_data_list ?: Cache::get('addons_data_list'));
    }

    /**
     * ============================
     * 获取 addons路径
     *
     * @return string
     * @Author  :   WYM
     * @Version :  1.0
     * @DateTime: 2024-06-18 16:03:49
     * ============================
     */
    public function getAddonsPath(): string
    {
        // 初始化插件目录
        $addons_path = $this->app->getRootPath() . 'addons' . DS;
        // 如果插件目录不存在则创建
        if (!is_dir($addons_path)) {
            @mkdir($addons_path, 0755, true);
        }

        return $addons_path;
    }

    /**
     * ============================
     * 自动载入钩子插件
     *
     * @return void
     * @Author  :   WYM
     * @Version :  1.0
     * @DateTime: 2024-06-18 16:03:02
     * ============================
     */
    private function autoload(): void
    {
        // 是否处理自动载入
        if (Config::get('addons.autoload', true)) {
            $config = Config::get('addons');
            // 读取插件目录及钩子列表
            $base = get_class_methods('\\wym\\addons\\Addons');
            $base = array_merge($base, ['init', 'initialize', 'install', 'uninstall', 'enabled', 'disabled']);
            // 读取插件目录中的php文件
            foreach (glob($this->getAddonsPath() . '*/*.php') as $addons_file) {
                // 格式化路径信息
                $info = pathinfo($addons_file);
                // 获取插件目录名
                $name = pathinfo($info['dirname'], PATHINFO_FILENAME);
                // 找到插件入口文件
                if (Str::lower($info['filename']) === 'plugin') {
                    // 读取出所有公共方法
                    if (!class_exists('\\addons\\' . $name . '\\' . $info['filename'])) {
                        continue;
                    }
                    $methods = get_class_methods('\\addons\\' . $name . '\\' . $info['filename']);
                    $ini     = $info['dirname'] . DS . 'plugin.ini';

                    if (!is_file($ini)) {
                        continue;
                    }
                    $addon_config = parse_ini_file($ini, true, INI_SCANNER_TYPED) ?: [];

                    $this->addons_data[]                                  = $addon_config['name'];
                    $this->addons_data_list[$addon_config['name']]        = $addon_config;
                    $this->addons_data_list_config[$addon_config['name']] = include $this->getAddonsPath(
                        ) . $addon_config['name'] . '/config.php';
                    // 跟插件基类方法做比对，得到差异结果
                    setAddonConfig($config, $methods, $base, $name);
                }
            }
            //插件配置信息保存到缓存
            Cache::set('addons_config', $config);
            //插件列表
            Cache::set('addons_data', $this->addons_data);
            //插件ini列表
            Cache::set('addons_data_list', $this->addons_data_list);
            //插件config列表
            Cache::set('addons_data_list_config', $this->addons_data_list_config);
            Config::set($config, 'addons');
        }
    }

    private function loadLang(): void
    {
        // 加载系统语言包
        Lang::load([
            $this->app->getRootPath() . '/vendor/wym/addons/src/lang/zh-cn.php',
        ]);
        // 加载应用默认语言包
        $this->app->loadLangPack();
    }

    /**
     * ============================
     * 挂载插件事件
     *
     * @Author  :   WYM
     * @Version :  1.0
     * @DateTime: 2024-06-18 16:00:50
     * ============================
     */
    private function loadEvent(): void
    {
        $hooks = $this->app->isDebug() ? [] : Cache::get('hooks', []);

        if (empty($hooks)) {
            $hooks = (array)Config::get('addons.hooks', []);
            // 初始化钩子
            foreach ($hooks as $key => $values) {
                if (is_string($values)) {
                    $values = explode(',', $values);
                } else {
                    $values = (array)$values;
                }
                $hooks[$key] = array_filter(array_map(function ($v) use ($key) {
                    return [getAddonsClass($v), $key];
                }, $values));
            }
            Cache::set('hooks', $hooks);
        }
        Event::listenEvents($hooks);
        //如果在插件中有定义 AddonsInit，则直接执行
        if (isset($hooks['AddonsInit'])) {
            foreach ($hooks['AddonsInit'] as $v) {
                Event::trigger('AddonsInit', $v);
            }
        }
    }

    /**
     * ============================
     * 挂载插件服务
     *
     * @Author  :   WYM
     * @Version :  1.0
     * @DateTime: 2024-06-18 16:00:32
     * ============================
     */
    private function loadService(): void
    {
        $results = scandir($this->addons_path);
        $bind    = [];

        foreach ($results as $name) {
            if ($name === '.' || $name === '..') {
                continue;
            }

            if (is_file($this->addons_path . $name)) {
                continue;
            }
            $addonDir = $this->addons_path . $name . DIRECTORY_SEPARATOR;

            if (!is_dir($addonDir)) {
                continue;
            }

            if (!is_file($addonDir . 'Plugin.php')) {
                continue;
            }

            $service_file = $addonDir . 'service.ini';

            if (!is_file($service_file)) {
                continue;
            }
            $info = parse_ini_file($service_file, true, INI_SCANNER_TYPED) ?: [];

            if ($info) {
                $this->app->register(array_shift($info));
            }
            $bind = array_merge($bind, $info);
        }
        $this->app->bind($bind);
    }

    /**
     * ============================
     * 加载插件命令
     *
     * @Author  :   WYM
     * @Version :  1.0
     * @DateTime: 2024-06-18 16:20:18
     * ============================
     */
    private function loadCommand(): void
    {
        $commands = [
            'addons:app'        => Command\App::class,
            'addons:config'     => Command\Config::class,
            'addons:controller' => Command\Controller::class,
            'addons:install'    => Command\Install::class,
            'addons:menu'       => Command\Menu::class,
            'addons:other'      => Command\Other::class,
            'addons:plugin'     => Command\Plugin::class,
            'addons:uninstall'  => Command\Uninstall::class,
            'addons:view'       => Command\View::class,
            'addons:command'    => Command\Command::class,
            'addons:commands'   => Command\Commands::class,
        ];
        // 定义要遍历的目录
        $directory = new DirectoryIterator($this->addons_path);
        // 遍历目录并打印信息
        foreach ($directory as $fileinfo) {
            if ($fileinfo->isDot()) {
                continue;
            }
            if ($fileinfo->isDir()) {
                $command_file = $fileinfo->getPathname() . DS . 'command.php';

                if (is_file($command_file)) {
                    $command = include_once $command_file;

                    if (is_array($command)) {
                        $commands = array_merge($commands, $command);
                    }
                }
            }
        }
        $this->commands($commands);
    }

    /**
     * ============================
     * 加载插件内部第三方类库
     *
     * @param mixed $addonsName 插件名称或插件数组
     *
     * @return bool
     * @Author  :   WYM
     * @Version :  1.0
     * @DateTime: 2024-06-18 16:03:23
     * ============================
     */
    public function vendorAutoload(mixed $addonsName): bool
    {
        //插件全局类库
        if (is_array($addonsName)) {
            foreach ($addonsName as $item) {
                $autoload_file = app()->addons->getAddonsPath() . $item['name'] . '/vendor/autoload.php';

                if (file_exists($autoload_file)) {
                    require_once $autoload_file;
                }
            }
        } else {
            //插件私有类库
            $autoload_file = app()->addons->getAddonsPath() . $addonsName . '/vendor/autoload.php';

            if (file_exists($autoload_file)) {
                require_once $autoload_file;
            }
        }

        return true;
    }

    public function boot(): void
    {
        $commands = [
            'addons:app'        => Command\App::class,
            'addons:config'     => Command\Config::class,
            'addons:controller' => Command\Controller::class,
            'addons:install'    => Command\Install::class,
            'addons:menu'       => Command\Menu::class,
            'addons:other'      => Command\Other::class,
            'addons:plugin'     => Command\Plugin::class,
            'addons:uninstall'  => Command\Uninstall::class,
            'addons:upgrade'    => Command\Upgrade::class,
            'addons:view'       => Command\View::class,
        ];
        Console::starting(function (Console $console) use ($commands) {
            foreach ($commands as $key => $command) {
                $console->addCommand($command, is_numeric($key) ? '' : $key);
            }
        });
        //注册HttpRun事件监听,触发后注册全局中间件到开始位置
        $this->registerRoutes(function (Route $route) {
            // 路由脚本
            $execute = '\\wym\\addons\\Route::execute';

            // 注册插件公共中间件
            $middleware = $this->app->addons->getAddonsPath() . 'middleware.php';
            if (is_file($middleware)) {
                $this->app->middleware->import(include $middleware, 'route');
            }

            // 注册控制器路由
            $route->rule('addons/:addon/[:controller]/[:action]', $execute)
                  ->middleware(\wym\addons\middleware\Addons::class);

            // 自定义路由
            $routes = (array)config('addons.route', []);

            foreach ($routes as $key => $val) {
                if (!$val) {
                    continue;
                }

                if (is_array($val)) {
                    $domain = $val['domain'];
                    $rules  = [];

                    foreach ($val['rule'] as $k => $rule) {
                        [$addon, $controller, $action] = explode('/', $rule);
                        $rules[$k] = [
                            'addons'     => $addon,
                            'controller' => $controller,
                            'action'     => $action,
                            'domain'     => 1,
                        ];
                    }
                    $route->domain($domain, function () use ($rules, $route, $execute) {
                        // 动态注册域名的路由规则
                        foreach ($rules as $k => $rule) {
                            $route->rule($k, $execute)
                                  ->name($k)
                                  ->completeMatch()
                                  ->append($rule);
                        }
                    });
                } else {
                    [$addon, $controller, $action] = explode('/', $val);
                    $route->rule($key, $execute)
                          ->name($key)
                          ->completeMatch()
                          ->append([
                              'addons'     => $addon,
                              'controller' => $controller,
                              'action'     => $action,
                          ]);
                }
            }
        });
    }
}
