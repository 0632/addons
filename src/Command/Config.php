<?php

declare(strict_types=1);

namespace wym\addons\Command;

use think\console\Input;
use think\console\Output;
use wym\addons\Addon;

class Config extends Addon
{
    protected $type = 'Addons';

    public function configure(): void
    {
        $this->setName('addons:config')
             ->setDescription('send config to config folder');
    }

    protected function execute(Input $input, Output $output): void
    {
        $filename = config_path() . 'addons.php';
        //判断目录是否存在
        if (!file_exists(config_path())) {
            mkdir(config_path(), 0755, true);
        }
        $info = $this->type . ':The config file ' . str_replace('.php', '', str_replace(root_path(), '', $filename));
        parent::write($filename, function ($content) {
            return $content;
        }, $info, $output, $this);
    }
}
