<?php

declare(strict_types=1);

namespace wym\addons\Command;

use think\console\input\Argument;

class Uninstall extends Install
{
    protected $type = 'Uninstall';

    protected function configure(): void
    {
        $this->setName('addons:uninstall')
             ->addArgument('plugin', Argument::REQUIRED, 'plugin name .')
             ->setDescription('Custom plugin uninstall');
    }
}
