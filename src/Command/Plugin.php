<?php

declare (strict_types=1);

namespace wym\addons\Command;

use think\console\Input;
use think\console\input\Argument;
use think\console\Output;
use wym\addons\Addon;

class Plugin extends Addon
{
    protected $type = 'Plugin';

    protected function configure(): void
    {
        $this->setName('addons:plugin')
             ->addArgument('plugin', Argument::REQUIRED, 'plugin name .')
             ->setDescription('Custom plugin');
    }

    protected function execute(Input $input, Output $output): void
    {
        $basePath = $this->app->addons->getAddonsPath();
        $plugin   = $input->getArgument('plugin') ?: '';

        $pluginPath = $basePath . $plugin;
        $this->checkDirBuild($pluginPath);

        $filename = $pluginPath . DIRECTORY_SEPARATOR . $this->type . '.php';
        $info     = $this->type . ':' . str_replace('.php', '', str_replace(root_path(), '', $filename));
        parent::write($filename, function ($content) use ($plugin) {
            return str_replace(['{%time%}', '{%plugin%}'], [date('Y-m-d H:i:s'), $plugin], $content);
        }, $info, $output, $this);
    }
}
