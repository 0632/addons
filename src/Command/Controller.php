<?php

declare(strict_types=1);

namespace wym\addons\Command;

use think\helper\Str;

class Controller extends Commands
{
    protected $type = 'Controller';

    protected function configure(): void
    {
        parent::configure();
        $this->setName('addons:controller')
             ->setDescription('Custom plugin controller');
    }

    protected function getClassName(string $name): string
    {
        if (str_contains($name, '\\')) {
            return $name;
        }
        $plugin = $name;

        if (strpos($name, '@')) {
            [$plugin, $name] = explode('@', $name);
        } else {
            $name = 'Index';
        }

        if (str_contains($plugin, '/')) {
            $plugin = str_replace('/', '\\', $plugin);
        }

        return $this->getNamespace($plugin) . '\\' . Str::studly($name);
    }

}
