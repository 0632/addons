<?php

declare(strict_types=1);

namespace wym\addons\Command;

use think\console\Input;
use think\console\Output;
use think\helper\Str;
use wym\addons\Addon;

class Commands extends Addon
{
    protected $type = 'Commands';

    protected function configure(): void
    {
        parent::configure();
        $this->setName('addons:commands')
             ->setDescription('Custom plugin commands');
    }

    protected function execute(Input $input, Output $output): void
    {
        parent::execute($input, $output);
        $plugin = trim($input->getArgument('name'));

        $classname = $this->getClassName($plugin);
        $namespace = trim(implode('\\', array_slice(explode('\\', $classname), 0, -1)), '\\');

        $filename = $this->getPathName($classname);

        if (is_file($filename)) {
            $content = file_get_contents($filename);
            $content = str_replace(['{%namespace%}'], [$namespace], $content);

            file_put_contents($filename, $content);
        }
    }

    protected function getNamespace(string $app): string
    {
        return parent::getNamespace($app) . '\\' . Str::lower($this->type);
    }

    protected function getClassName(string $name): string
    {
        if (str_contains($name, '\\')) {
            return $name;
        }
        $plugin = $name;

        if (strpos($name, '@')) {
            [$plugin, $name] = explode('@', $name);
        } else {
            $name = 'Command';
        }

        if (str_contains($plugin, '/')) {
            $plugin = str_replace('/', '\\', $plugin);
        }

        return $this->getNamespace($plugin) . '\\' . Str::studly($name);
    }

    protected function getPathName(string $name): string
    {
        $name = str_replace('addons\\', '', $name);

        return $this->app->addons->getAddonsPath() . ltrim(str_replace('\\', '/', $name), '/') . '.php';
    }
}
