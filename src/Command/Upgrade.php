<?php

declare(strict_types=1);

namespace wym\addons\Command;

use think\console\input\Argument;

class Upgrade extends Install
{
    protected $type = 'Upgrade';

    protected function configure(): void
    {
        $this->setName('addons:upgrade')
             ->addArgument('plugin', Argument::REQUIRED, 'plugin name .')
             ->setDescription('Custom plugin upgrade');
    }
}
