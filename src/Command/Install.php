<?php

declare(strict_types=1);

namespace wym\addons\Command;

use think\console\Input;
use think\console\input\Argument;
use think\console\Output;
use wym\addons\Addon;
use think\helper\Str;

class Install extends Addon
{
    protected $type = 'Install';

    protected function configure(): void
    {
        $this->setName('addons:install')
             ->addArgument('plugin', Argument::REQUIRED, 'plugin name .')
             ->setDescription('Custom plugin install');
    }

    protected function execute(Input $input, Output $output): void
    {
        $basePath = $this->app->addons->getAddonsPath();
        $plugin   = $input->getArgument('plugin') ?: '';

        $pluginPath = $basePath . $plugin;
        $this->checkDirBuild($pluginPath);

        $filename = $pluginPath . DIRECTORY_SEPARATOR . Str::lower($this->type) . '.sql';
        $info     = $this->type . ':' . str_replace('.sql', '', str_replace(root_path(), '', $filename));
        parent::write($filename, function ($content) use ($plugin) {
            return str_replace(['{%plugin%}'], [$plugin], $content);
        }, $info, $output, $this);
    }
}
