<?php

namespace wym\addons;

use think\console\command\Make;

class Addon extends Make
{

    protected $type;

    /**
     * 获取模板
     *
     * @return string
     */
    protected function getStub(): string
    {
        return __DIR__ . DIRECTORY_SEPARATOR . 'Stubs' . DIRECTORY_SEPARATOR . $this->type . '.stub';
    }

    /**
     * 获取类命名空间
     *
     * @param string $app
     *
     * @return string
     */
    protected function getNamespace(string $app): string
    {
        return 'addons' . ($app ? '\\' . $app : '');
    }

    /**
     * 创建目录
     *
     * @access protected
     *
     * @param string $dirname 目录名称
     *
     * @return void
     */
    protected function checkDirBuild(string $dirname): void
    {
        if (!is_dir($dirname)) {
            mkdir($dirname, 0755, true);
        }
    }

    protected function write($filename, $content, $info, $output, $that): void
    {
        if (!is_file($filename)) {
            file_put_contents($filename, $content(file_get_contents($that->getStub())));
            $info = '<info>' . $info . ' created successfully.</info>';
        } else {
            $info = '<error>' . $info . ' already exists!</error>';
        }
        $output->writeln($info);
    }
}