<?php
declare (strict_types=1);

namespace wym\addons\middleware;

use Closure;
use think\App;
use think\Request;

class Addons
{
    protected App $app;

    public function __construct(App $app)
    {
        $this->app = $app;
    }

    /**
     * 插件中间件
     *
     * @param         $request
     * @param Closure $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next): mixed
    {
        hook('addon_middleware', $request);

        return $next($request);
    }
}
