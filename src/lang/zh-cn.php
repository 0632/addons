<?php

// 插件中文语言包
return [
    'addon name is not right'                    => '插件%s名字不正确',
    'require addon name is not right'            => '依赖插件%s名字不正确',
    'addon %s not found'                         => '插件%s未找到',
    'addon %s is disabled'                       => '插件%s已禁用',
    'require addon %s is disabled'               => '依赖插件%s已禁用',
    'addon controller %s not found'              => '插件控制器%s未找到',
    'addon action %s not found'                  => '插件控制器方法%s未找到',
    'addon can not be empty'                     => '插件不能为空',
    'require addon can not be empty'             => '依赖插件%s不能为空',
    'failed to write plugin config'              => '未能写入插件配置',
    'file does not have write permission'        => '文件没有写入权限',
    'addons %s is already installed'             => '插件%s已安装',
    'addons %s is already not installed'         => '插件%s未安装',
    'require addons %s is already not installed' => '依赖插件%s未安装',
    'addons %s is not ready'                     => '插件%s还没有准备好',
    'require addon %s version must be %s'        => '依赖插件%s版本必须%s',

    'addon name'        => '插件标识',
    'addon title'       => '插件名称',
    'addon description' => '插件介绍',
    'addon author'      => '插件作者',
    'addon version'     => '插件版本',
    'publish_time'      => '发布时间',
    'install'           => '安装',
    'uninstall'         => '卸载',
    'upgrade'            => '升级',
    'enabled'           => '启用',
    'disabled'          => '禁用',
];
